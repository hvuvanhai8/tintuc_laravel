<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::group(['namespace'=>'Admin'],function(){
//     Route::group(['prefix'=>'login'],function(){
//         Route::get('/','LoginController@getLogin')->name('login');
//         Route::get('/dangnhap','LoginController@postLogin')->name('dangnhap');


//     });
// });


// route đăng nhập
Route::get('/login', 'Admin\LoginController@getLogin')->name('admin.login');
Route::post('/postlogin', 'Admin\LoginController@postLogin')->name('admin.post.login');

Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {

    Route::get('/dashboard', function () {
        return view('backend.index');
    });

    Auth::routes();







// logout admin
Route::get('/getlogout', 'Admin\LoginController@getLogout')->name('admin.logout');

});


// route frontend
// Route::get('/', function () {
//     return view('frontend.page.home');
// });

Route::get('/', 'Web\HomeController@getIndex')->name('home');
Route::get('/loai-tin/{id}/{TenKhongDau}.html', 'Web\HomeController@getCategory')->name('category');
Route::get('/tin-tuc-chi-tiet/{id}/{TieuDeKhongDau}.html', 'Web\HomeController@getNewDetails')->name('newdetails'); 