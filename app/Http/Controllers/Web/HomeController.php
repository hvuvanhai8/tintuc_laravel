<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TinTuc;
Use App\Models\TheLoai;
Use App\Models\LoaiTin;

class HomeController extends Controller
{
    //
    public function getIndex(){
        $tin = TinTuc::select('id','TieuDe','TomTat','NoiDung','Hinh','TieuDeKhongDau','created_at')->orderBy('id', 'desc')->limit(16)->get();
        
        $tinhot = TinTuc::select('id','TieuDe','TomTat','NoiDung','Hinh','NoiBat','TieuDeKhongDau')->inRandomOrder()->where('NoiBat',1)->limit(3)->get();
        // dd($tinhot);
        // $hai  = 12;
        return view('frontend.page.home', compact('tin','tinhot'));
    }


    public function getCategory($id){
        
        $loaitin = LoaiTin::find($id);
        // dd($loaitin);
        $tintuc = TinTuc::where('idLoaiTin',$id)->orderBy('id', 'desc')->paginate(12);
        return view('frontend.page.category',compact('loaitin','tintuc'));
    }

    public function getNewDetails($id){
        $tintuc = TinTuc::find($id);
        $tinnoibat = TinTuc::where('NoiBat',1)->limit(4)->get();
        $tinlienquan = TinTuc::where('idLoaiTin',$tintuc->idLoaiTin)->limit(4)->get();
        $theloai = TheLoai::all();

        return view('frontend.page.details',compact('tintuc','tinlienquan','tinnoibat','theloai'));
    }
}
