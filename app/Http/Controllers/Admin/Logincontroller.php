<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class Logincontroller extends Controller
{
    //
    public function getLogin()
    { 
        return view('backend.login.login');
    }

    public function postLogin(Request $request)
    {   
        $login = $request->only('email', 'password');

        if (Auth::attempt($login)) {
            return redirect('admin/dashboard');
        }
        else 

        return redirect()->back()->with(['error'=>' Email khoặc mật khẩu không đúng ']);

        return view('admin.login');
    }

    public function getLogout(Request $request)
    {
        Auth::logout();

        return view('backend.login.login');
    }


}
