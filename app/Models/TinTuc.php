<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TinTuc extends Model
{
    //
    protected $table = 'tintuc';
    public $timestamps = true;

    public function loaitin(){
        return $this->belongsTo('App\Models\LoaiTin','idLoaiTin','id');
    }

    
}
