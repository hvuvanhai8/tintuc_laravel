<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Users;
use App\Models\TheLoai;
use App\Models\LoaiTin;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        view()->composer('frontend/layout/header',function($view){

            
            $theloai = TheLoai::all();
            $loaitin = LoaiTin::all();
            // dd($theloai);
            
            $view->with(['theloai'=>$theloai,'loaitin'=>$loaitin]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
