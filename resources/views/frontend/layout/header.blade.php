

<header>
		<div class="container panel-body">
			<div class="row" id="csstop">
				<div class="col-sm-4 panel-body">
					<a href="/"><img class="img-responsive" src="images/logo/logo.png" ></a>
				</div>
				<div class="col-sm-4  text-center">
				</div>
				<div class="col-sm-4 pull-left" style="padding-top:30px;">
				</div>
			</div>
		</div>
</header>
<section class="abc">
		<div class="container">
			<div class="row" >
		
				<nav class="navbar navbar-inverse" >
					<div class="container-fluid" id="menu">
						<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>                        
						</button>
						</div>
						<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav">
							<li><a href="/"><i class="fas fa-home"></i></a></li>
							@foreach($theloai as $tl)
								
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ $tl->Ten }} @if(count($tl->loaitin) > 0 )<span class="caret">@endif</span></a>
									@if(count($tl->loaitin) > 0 )
									<ul class="dropdown-menu">
										@foreach($tl->loaitin as $lt)
										<li><a href="/loai-tin/{{$lt->id}}/{{$lt->TenKhongDau}}.html">{{ $lt->Ten }}</a></li>
										@endforeach
									</ul>
									@endif
								</li>
									
							@endforeach
						</ul>
						<!-- <ul class="nav navbar-nav navbar-right">
							<li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
							<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
						</ul> -->
						</div>
					</div>
				</nav>
				
			</div>
			
		</div>
</section>