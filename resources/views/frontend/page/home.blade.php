@extends('frontend.layout.master')
@section('content')

<?php 


?>
    
<section id="def">
    <div class="container">
        <div class="row">
           
        </div>
        <div class="row">
            <div class="col-sm-4 panel-default left_home">
                <div class="col-sm-12 text-left">
                    <h3>Tin nóng nhất</h3>
                </div>
                @foreach($tinhot as $hot)
                <div class="col-sm-12 col-xs-6">
                    <p><a href="/tin-tuc-chi-tiet/{{$hot->id}}/{{$hot->TieuDeKhongDau}}.html"><img src="images/{{ $hot->Hinh }}" class="img-responsive"></a></p>
                    <p><b><a href="/tin-tuc-chi-tiet/{{$hot->id}}/{{$hot->TieuDeKhongDau}}.html">{{ $hot->TieuDe }}</a></b></p>
                    <p><i>{{ $hot->TomTat }}</i></p>
                </div>
                @endforeach
            </div>
            <div class="col-sm-8 panel-default right_home">
                <div class="col-sm-12 text-left">
                    <h3>Tin mới nhất</h3>
                </div>
                @foreach($tin as $ti)
                <div class="col-sm-3 col-xs-6">
                    <p><a href="/tin-tuc-chi-tiet/{{$ti->id}}/{{$ti->TieuDeKhongDau}}.html"><img src="images/{{ $ti->Hinh }}" class="img-responsive"></a></p>
                    <p><time>{{ today_yesterday_bds(strtotime($ti->created_at)) }} </time></p>
                    <p><b><a href="/tin-tuc-chi-tiet/{{$ti->id}}/{{$ti->TieuDeKhongDau}}.html">{{ $ti->TieuDe }}</a></b></p>
                    <p><i>{{ $ti->TomTat }}</i></p>
                </div>
                @endforeach
            </div>
        </div>
        <div class="row">
        </div>
    </div>


</section>


@endsection