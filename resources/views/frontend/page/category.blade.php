@extends('frontend.layout.master')
@section('content')


<section id="def">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-left">
                <h3>{{ $loaitin->Ten}}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9 panel-default">
                @foreach($tintuc as $tt)
                <div class="col-sm-12">
                    <div class="col-sm-4"><p><a href="/tintuc/{{$tt->id}}/{{$tt->TieuDeKhongDau}}.html"><img src="images/{{ $tt->Hinh }}" class="img-responsive"></a></p></div>
                    <div class="col-sm-8"><p><b><a href="/tintuc/{{$tt->id}}/{{$tt->TieuDeKhongDau}}.html">{{ $tt->TieuDe }}</a></b></p>
                    <p><i>{{ $tt->TomTat }}</i></p></div>
                </div>
                @endforeach
                <div class="row text-center">
                    {{ $tintuc->links() }}
                </div>
            </div>
            <div class="col-sm-3 panel-default">
            </div>
        </div>
        
    </div>
</section>


@endsection